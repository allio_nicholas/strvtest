//
//  ForecastTableViewCell.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 12/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell {
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var descriptionWeatherLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
}
