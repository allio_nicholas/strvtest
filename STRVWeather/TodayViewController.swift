//
//  TodayViewController.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 06/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase

class TodayViewController: UITableViewController {
    //Top section
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentCityLabel: UILabel!
    @IBOutlet weak var temperatureWeatherDescLabel: UILabel!
    
    //Mid section
    @IBOutlet weak var humidityPercentageLabel: UILabel!
    @IBOutlet weak var rainQuantityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    
    let weatherHandler = OpenWeatherHandler()
    var firebaseRef: FIRDatabaseReference?
    
    var currentLocation: CLLocation?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        firebaseRef = FIRDatabase.database().reference(withPath: "current-weather")
        
        firebaseRef?.observeSingleEvent(of: .value, with: { (snapshot) in
            let snapshotValue = snapshot.value as! [String: AnyObject]
            let weatherObj = snapshotValue["weather"] as! [String:AnyObject]
            let locationString = weatherObj["location"] as? String ?? "-, -"
            let tmpWeather = Weather()
            tmpWeather.temperature = weatherObj["temperature"] as? Float
            
            DispatchQueue.main.async {
                self.currentCityLabel.text = locationString
                self.updateWeatherUI(weather: tmpWeather)
            }
        })
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 80)
        indicator.startAnimating()
        
        let loadingLabel = UILabel(frame: CGRect(x: self.tableView.center.x - 40, y: self.tableView.center.y - 10, width: 80, height: 21))
        let attributedString = NSAttributedString(string: "Loading...", attributes: [NSFontAttributeName:UIFont(name: "ProximaNova-Semibold", size: 18)!, NSForegroundColorAttributeName:UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)])
        loadingLabel.attributedText = attributedString
        
        self.tableView.backgroundView = UIView(frame: self.tableView.frame)
        self.tableView.backgroundView?.addSubview(loadingLabel)
        self.tableView.backgroundView?.addSubview(indicator)
        self.tableView.separatorStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleLocation(notification:)), name: NSNotification.Name("location"), object: nil)
    }
    
    func handleLocation(notification: Notification) {
        currentLocation = notification.object as? CLLocation
        let geocoder = CLGeocoder()
        var positionString = ""
        geocoder.reverseGeocodeLocation(currentLocation!, completionHandler: { (placemarks, error) in
            if let placemark = placemarks?[0] {
                positionString = "\(placemark.addressDictionary!["City"]!), \(placemark.addressDictionary!["Country"]!)"
            }
            self.currentCityLabel.text = positionString
        })
        
        self.weatherHandler.getCurrentWeather(coordinates: currentLocation!.coordinate, { (result) in
            if let result = result {
                DispatchQueue.main.async {
                    self.tableView.backgroundView = nil
                    self.tableView.separatorStyle = .singleLine
                    self.updateWeatherUI(weather: result)
                }
                
                DispatchQueue.global(qos: .background).async {
                    let weatherObject = [
                        "location": positionString,
                        "temperature": result.temperature as Any
                    ]
                    
                    self.firebaseRef!.child("weather").setValue(weatherObject)
                }
                
            }
        })
        NotificationCenter.default.removeObserver(self)
    }

    func updateWeatherUI(weather: Weather) {
        let temperature = weather.temperature != nil ? "\(weather.temperature!)" : "-"
        let description = weather.desc != nil ? "\(weather.desc!)" : "-"
        let humidity = weather.humidity != nil ? "\(weather.humidity!)" : "-"
        let rain = weather.rainQuantity != nil ? "\(weather.rainQuantity!)" : "-"
        let pressure = weather.pressure != nil ? "\(weather.pressure!)" : "-"
        let windSpeed = weather.windSpeed != nil ? "\(weather.windSpeed!)" : "-"        
        
        self.temperatureWeatherDescLabel.text = "\(temperature)°C | \(description)"
        self.humidityPercentageLabel.text = "\(humidity) %"
        self.rainQuantityLabel.text = "\(rain) mm"
        self.pressureLabel.text = "\(pressure) hPa"
        self.windSpeedLabel.text = "\(windSpeed) km/h"
        self.windDirectionLabel.text = "\(weather.returnDirectionString())"
        
        if let imageName = WeatherParser().nameIconFromWeatherCode(code: weather.code) {
            self.weatherImage.image = UIImage(named: imageName)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleLocation(notification:)), name: NSNotification.Name("location"), object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func shareButtonPressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["This is your forecast"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = sender as! UIButton
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return (2 * (self.tableView.frame.height - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height - UIApplication.shared.statusBarFrame.height)) / 4
        } else {
            return (1 * (self.tableView.frame.height - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height - UIApplication.shared.statusBarFrame.height)) / 4
        }
    }

}

