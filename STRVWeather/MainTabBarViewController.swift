//
//  MainTabBarViewController.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 07/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MainTabBarViewController: UITabBarController, CLLocationManagerDelegate {
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        NotificationCenter.default.post(name: NSNotification.Name("location"), object: locations.last)
    }

}
