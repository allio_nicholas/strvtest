//
//  ForecastViewController.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 06/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseDatabase

enum WeekDays: Int {
    case Sunday
    case Monday
    case Tuesday
    case Wednesday
    case Thursday
    case Friday
    case Saturday
}

class ForecastViewController: UITableViewController {
    let weatherHandler = OpenWeatherHandler()
    var forecastList = [Weather]()
    
    var firebaseRefForecast: FIRDatabaseReference?
    var today: WeekDays?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firebaseRefForecast = FIRDatabase.database().reference(withPath: "current-weather")
        
        firebaseRefForecast?.observeSingleEvent(of: .value, with: { (snapshot) in
            let snapshotValue = snapshot.value as! [String: AnyObject]
            let weatherObj = snapshotValue["weather"] as! [String:AnyObject]
            let locationString = weatherObj["location"] as? String ?? "-, -"
            
            DispatchQueue.main.async {
                self.navigationItem.title = locationString
                self.tableView.reloadData()
            }
        })
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.center = CGPoint(x: self.view.center.x, y: self.view.center.y - 80)
        indicator.startAnimating()
        
        let loadingLabel = UILabel(frame: CGRect(x: self.tableView.center.x - 40, y: self.tableView.center.y - 10, width: 80, height: 21))
        let attributedString = NSAttributedString(string: "Loading...", attributes: [NSFontAttributeName:UIFont(name: "ProximaNova-Semibold", size: 18)!, NSForegroundColorAttributeName:UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)])
        loadingLabel.attributedText = attributedString
        
        self.tableView.backgroundView = UIView(frame: self.tableView.frame)
        self.tableView.backgroundView?.addSubview(loadingLabel)
        self.tableView.backgroundView?.addSubview(indicator)
        self.tableView.separatorStyle = .none
        
        let todayDate = Date()
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        let component = calendar?.components(.weekday, from: todayDate)
        let weekDay = component?.weekday
        today = WeekDays(rawValue: weekDay! - 1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleLocation(notification:)), name: NSNotification.Name("location"), object: nil)
    }
    
    func handleLocation(notification: Notification) {
        let location = notification.object as! CLLocation
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) in
            let placemark = placemarks?[0]
            self.navigationItem.title = "\(placemark!.addressDictionary!["City"]!)"
        })
        
        self.weatherHandler.getForecastWeather(coordinates: location.coordinate, { (result) in
            if let result = result {
                self.forecastList = result
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        })
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleLocation(notification:)), name: NSNotification.Name("location"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    // TableView methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.forecastList.count > 0 {
            tableView.backgroundView = nil
            tableView.separatorStyle = .singleLine
            return 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecastList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as! ForecastTableViewCell
        let currentForecast = self.forecastList[indexPath.row]
        
        let temperature = currentForecast.temperature != nil ? "\(Int(currentForecast.temperature!))" : "-"
        cell.temperatureLabel.text = "\(temperature)°"
        
        let description = currentForecast.desc != nil ? "\(currentForecast.desc!)" : "-"
        cell.descriptionWeatherLabel.text = "\(description)"
        
        if let imageName = WeatherParser().nameIconFromWeatherCode(code: currentForecast.code!) {
            cell.weatherImage.image = UIImage(named: imageName)
        }
        
        let currentDayCell = WeekDays(rawValue: (today!.rawValue + indexPath.row) % 7)
        var dayString = ""
        
        switch currentDayCell! {
        case .Sunday:
            dayString = "Sunday"
        case .Monday:
            dayString = "Monday"
        case .Tuesday:
            dayString = "Tuesday"
        case .Wednesday:
            dayString = "Wednesday"
        case .Thursday:
            dayString = "Thursday"
        case .Friday:
            dayString = "Friday"
        case .Saturday:
            dayString = "Saturday"
        }
        
        cell.dayLabel.text = dayString
        
        return cell
    }

}

