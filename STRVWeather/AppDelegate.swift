//
//  AppDelegate.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 06/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FIRApp.configure()
        FIRDatabase.database().persistenceEnabled = true
        
        let grayish = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
        let lightBlue = UIColor(red: 47/255, green: 145/255, blue: 255/255, alpha: 1.0)
        

        //NavigationBar
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName:UIFont(name: "ProximaNova-Semibold", size: 18)!, NSForegroundColorAttributeName:grayish]
        
        //TabBar
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name: "ProximaNova-Semibold", size: 10)!, NSBackgroundColorAttributeName:grayish], for: UIControlState())
        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name: "ProximaNova-Semibold", size: 10)!, NSBackgroundColorAttributeName:lightBlue], for: .selected)
        
        
        return true
    }
}

