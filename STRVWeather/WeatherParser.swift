//
//  WeatherParser.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 07/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit

struct OWCostants {
    static let list = "list"
    
    static let main = "main"
    
    static let weather = "weather"
    static let code = "id"

    static let temperature = "temp"
    static let dayTemperature = "day"
    static let pressure = "pressure"
    static let humidity = "humidity"
    
    static let rain = "rain"
    static let threeHours = "3h"
    
    static let wind = "wind"
    static let windSpeed = "speed"
    static let windDegrees = "deg"
}

class WeatherParser: NSObject {
    
    func currentWeatherFrom(data: Data) throws -> Weather {
        let weatherResult = Weather()
        let parsedData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
        
        let weather = parsedData[OWCostants.weather] as? [[String:AnyObject]]
        weatherResult.desc = weather?[0][OWCostants.main] as? String
        weatherResult.code = weather?[0][OWCostants.code] as? Int
        
        let tempAndPressure = parsedData[OWCostants.main] as? [String:AnyObject]
        weatherResult.temperature = tempAndPressure?[OWCostants.temperature] as? Float
        weatherResult.humidity = tempAndPressure?[OWCostants.humidity] as? Float
        weatherResult.pressure = tempAndPressure?[OWCostants.pressure] as? Float
        
        let rain = parsedData[OWCostants.rain] as? [String:AnyObject]
        weatherResult.rainQuantity = rain?[OWCostants.threeHours] as? Float
        
        let wind = parsedData[OWCostants.wind] as? [String:AnyObject]
        weatherResult.windSpeed = wind?[OWCostants.windSpeed] as? Float
        weatherResult.windDirection = wind?[OWCostants.windDegrees] as? Float
        
        return weatherResult
    }
    
    func forecastWeatherFrom(data: Data) throws -> [Weather] {
        var forecastResult = [Weather]()
        let parsedData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:AnyObject]
        
        if let dayList = parsedData[OWCostants.list] as? [[String:AnyObject]] {
            for day in dayList {
                let currentWeather = Weather()
                
                let temp = day[OWCostants.temperature] as? [String:AnyObject]
                currentWeather.temperature = temp?[OWCostants.dayTemperature] as? Float
                
                currentWeather.pressure = day[OWCostants.pressure] as? Float
                currentWeather.humidity = day[OWCostants.humidity] as? Float
                
                let weather = day[OWCostants.weather] as? [[String:AnyObject]]
                currentWeather.code = weather?[0][OWCostants.code] as? Int
                currentWeather.desc = weather?[0][OWCostants.main] as? String
                
                currentWeather.windSpeed = day[OWCostants.windSpeed] as? Float
                currentWeather.windDirection = day[OWCostants.windDegrees] as? Float
                currentWeather.rainQuantity = day[OWCostants.rain] as? Float
                
                forecastResult.append(currentWeather)
            }
        }
        
        return forecastResult
    }
    
    func nameIconFromWeatherCode(code: Int?) -> String? {
        if let code = code {
            switch code {
            case 200...299:
                return "YellowSun"
            case 300...599:
                return "Rain"
            case 700..<800:
                return "Cloudy_Big"
            case 800:
                return "YellowSun"
            case 801..<900:
                return "Cloudy_Big"
            case 900...909:
                return "Lightning_Big"
            default:
                return nil
            }
        } else {
            return nil
        }
    }

}
