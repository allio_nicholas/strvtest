//
//  OpenWeatherHandler.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 07/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit
import CoreLocation

class OpenWeatherHandler: NSObject {
    fileprivate let APIKey = "3c6d8fdb551914515b6397fde99054d9"
    fileprivate let units = "metric"
    fileprivate let urlCallCurrent = "http://api.openweathermap.org/data/2.5/weather?"
    fileprivate let urlCallForecast = "http://api.openweathermap.org/data/2.5/forecast/daily?cnt=7"
    
    fileprivate let session = URLSession.shared
    fileprivate let parser = WeatherParser()
    
    func getCurrentWeather(coordinates: CLLocationCoordinate2D, _ completion: @escaping (Weather?) -> ()) {
        let urlString = urlCallCurrent +
            "lat=\(coordinates.latitude)&lon=\(coordinates.longitude)" +
            "&units=\(units)" +
            "&APPID=\(APIKey)"
        
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard (error == nil) else {
                completion(nil)
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                completion(nil)
                return
            }
            
            guard let data = data else {
                completion(nil)
                return
            }
            
            DispatchQueue.global(qos: .background).async {
                do {
                    let result = try self.parser.currentWeatherFrom(data: data)
                    completion(result)
                } catch {
                    completion(nil)
                }
            }
        })
        task.resume()
    }
    
    func getForecastWeather(coordinates: CLLocationCoordinate2D, _ completion: @escaping ([Weather]?) -> ()) {
        let urlString = urlCallForecast +
            "&lat=\(coordinates.latitude)&lon=\(coordinates.longitude)" +
            "&units=\(units)" +
            "&APPID=\(APIKey)"
        
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            guard (error == nil) else {
                completion(nil)
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                completion(nil)
                return
            }
            
            guard let data = data else {
                completion(nil)
                return
            }
            
            DispatchQueue.global(qos: .background).async {
                do {
                    let result = try self.parser.forecastWeatherFrom(data: data)
                    completion(result)
                } catch {
                    completion(nil)
                }
            }
        })
        task.resume()
    }

}
