//
//  Weather.swift
//  STRVWeather
//
//  Created by Nicholas Allio on 07/12/2016.
//  Copyright © 2016 Nicholas Allio. All rights reserved.
//

import UIKit

class Weather: NSObject {
    var desc: String?
    var temperature: Float?
    var pressure: Float?
    var humidity: Float?
    var windSpeed: Float?
    var windDirection: Float?
    var rainQuantity: Float?
    
    var code: Int?
    
    func returnDirectionString() -> String {
        if let direction = self.windDirection{
            switch direction {
            case 348.75..<360:
                return "N"
            case 0..<11.25:
                return "N"
            case 11.25..<33.75:
                return "NNE"
            case 33.75..<56.25:
                return "NE"
            case 56.25..<78.75:
                return "ENE"
            case 78.75..<101.25:
                return "E"
            case 101.25..<123.75:
                return "ESE"
            case 123.75..<146.25:
                return "SE"
            case 146.25..<168.75:
                return "SSE"
            case 168.75..<191.25:
                return "S"
            case 191.25..<213.75:
                return "SSW"
            case 213.75..<236.25:
                return "SW"
            case 236.25..<258.75:
                return "WSW"
            case 258.75..<281.25:
                return "W"
            case 281.25..<303.75:
                return "WNW"
            case 303.75..<326.25:
                return "NW"
            case 326.25..<348.75:
                return "NNW"
            default:
                return "-"
            }
        } else {
            return "-"
        }
    }
}
